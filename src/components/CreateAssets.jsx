import * as React from "react";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import PopupModel from "./PopupModel";
import axios from "axios";

export default function CreateAssets({ getApi, getAssetType, getAssetTypeApi }) {
  const [open, setOpen] = React.useState(false);
  const [error, setError] = React.useState(false);

  const   handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    setError(false);
  };

  const handelSubmit = (e, assets) => {
    e.preventDefault();

    // console.log("assets",assets);
    const formData = new FormData();
    formData.append("assetType", assets.assetType);
    formData.append("price", assets.price);
    formData.append("purchaseon", assets.purchaseon);
    formData.append("model", assets.model);
    formData.append("attachment", assets.attachment);
    formData.append("description", assets.description);
    // console.log("formData",formData);
    
    if (
      !assets.assetType ||
      !assets.price ||
      !assets.purchaseon ||
      !assets.model ||
      !assets.attachment ||
      !assets.description
    ) {
      setError(true);
    } else {
      axios
        .post("http://localhost:4000/asset/createasset", formData)
        .then((res) => {
          getApi();
        })
        .catch((err) => console.log(err));
      handleClose();
    }
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        <AddIcon /> Create Assets
      </Button>

      {open && (
        <PopupModel
          open={open}
          error={error}
          handleClose={handleClose}
          handelSubmit={handelSubmit}
          Update={{}}
          getAssetType={getAssetType}
          getAssetTypeApi={getAssetTypeApi}
        />
      )}
    </div>
  );
}
