import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import axios from "axios";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import BorderColorIcon from "@mui/icons-material/BorderColor";

import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";

export default function CreateCategoryTable({
  getAssetType,
  getAssetTypeApi,
  setAssetTypeid,
}) {
  const handelDelete = (id) => {
    axios
      .delete(`http://localhost:4000/asset/dropcategories?assetstypeId=${id}`)
      .then((res) => {
        // console.log(res);
        getAssetTypeApi();
      })
      .catch((err) => {
        console.log(err);
      });
    // console.log(id);
  };

  return (
    <Paper>
      <TableContainer sx={{ maxHeight: "300px" }}>
        <Table aria-label="simple table" stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell>NO</TableCell>
              <TableCell>Category Name</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getAssetType.map((data, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{data.assetstype}</TableCell>
                  <TableCell>

                    <Tooltip title="Delete" arrow placement="top">
                      <IconButton
                        onClick={() =>
                          setAssetTypeid({
                            typeIndex: index,
                            typeId: data._id,
                            typeName: data.assetstype,
                          })
                        }
                        sx={{ marginRight: "20px" }}
                      >
                        <BorderColorIcon color="success" />
                      </IconButton>
                    </Tooltip>

                    <Tooltip title="Delete" arrow placement="top">
                      <IconButton onClick={() => handelDelete(data._id)}>
                        <DeleteForeverIcon color="error" />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
