import React from "react";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { useNavigate } from "react-router-dom";
import { List, ListItemIcon } from "@mui/material";

const SideNavigation = () => {
  const [selectedIndex, setSelectedIndex] = React.useState(1);

  const navigate = useNavigate();

  const handelAssetMange = () => {
    navigate("/asset");
  };

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };
  
  return (
    <div>
      <List>
        <ListItemButton
          selected={selectedIndex === 0}
          onClick={(event) => handleListItemClick(event, 0)}
          divider
          className="list"
        >
          <ListItemIcon>
            <ManageAccountsIcon />
          </ListItemIcon>
          <ListItemText
            primary="Asset Management"
            onClick={handelAssetMange}
          ></ListItemText>
        </ListItemButton>
      </List>
    </div>
  );
};

export default SideNavigation;
