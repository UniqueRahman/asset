import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { TextField } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import Box from "@mui/material/Box";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import CreateCategoryTabel from "./CreateCategoryTable";
import axios from "axios";
// import { useEffect } from "react";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

const assetsType = () => ({
  assetstype: "",
});

export default function CreateCategory({ getAssetType, getAssetTypeApi }) {
  const [assetType, setAssetType] = useState(assetsType());
  // console.log(asstype);
  const [open, setOpen] = useState(false);

  const [erroShow, setErroShow] = useState(false);
  //------------------------------------------------------------------------>
  const [assetTypeid, setAssetTypeid] = useState({
    typeIndex: "",
    typeId: "",
    typeName: "",
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setErroShow(false);
    setAssetTypeid({ typeIndex: "", typeId: "" });
    setAssetType(assetsType());
  };
  //------------------------------------------------------>

  const handleAssChange = (onChangeValue) => {
    const handleAssChange = { ...assetType };
    handleAssChange.assetstype = onChangeValue.target.value;
    setErroShow(false);
    setAssetType(handleAssChange);
  };

  const handleSubmitType = (e) => {
    e.preventDefault();
    if (assetTypeid.typeId !== "") {
      axios
        .put(
          `http://localhost:4000/asset/updatecategories?assetstypeId=${assetTypeid.typeId}&newassetstype=${assetType.assetstype}`
        )
        .then((res) => {
          getAssetTypeApi();
          setAssetType(assetsType());
          setAssetTypeid({ typeIndex: "", typeId: "" });
        })
        .catch((err) => console.log(err));
      // console.log(assTypeid.typeId);
    } else if (assetType.assetstype) {
      axios
        .post("http://localhost:4000/asset/createcategories", assetType)
        .then((res) => {
          // console.log(res.data);
          getAssetTypeApi();
        })
        .catch((err) => console.log(err));

      setAssetType(assetsType());
    } else {
      setErroShow(true);
    }
  };
  React.useEffect(() => {
    if (assetTypeid.typeIndex !== "") {
      const a = [...getAssetType];
      const handleAssChange = { ...assetType };
      handleAssChange.assetstype = a[assetTypeid.typeIndex].assetstype;
      setAssetType(handleAssChange);
    }
  }, [assetTypeid.typeIndex]); //eslint-disable-line

  return (
    <span className="addIcon">
      <AddCircleRoundedIcon onClick={handleClickOpen} />
      <BootstrapDialog aria-labelledby="customized-dialog-title" open={open}>
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Create Category
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <form onSubmit={handleSubmitType}>
            <Typography className="category-header">
              <div>
                <TextField
                  id="outlined-basic"
                  label="Category"
                  variant="outlined"
                  autoComplete="none"
                  value={assetType.assetstype}
                  required
                  onChange={(e) => handleAssChange(e)}
                />
                {erroShow && <div className="error">Category is Empty</div>}
              </div>
              <Button
                variant="outlined"
                type="submit"
                onClick={handleSubmitType}
              >
                <AddIcon />{" "}
                {assetTypeid.typeIndex !== ""
                  ? "Update Category"
                  : "Add Category"}
              </Button>
            </Typography>
          </form>
          <Box sx={{ width: "500px", height: "300px", marginTop: "50px" }}>
            <CreateCategoryTabel
              // asstype={asstype}
              getAssetType={getAssetType}
              getAssetTypeApi={getAssetTypeApi}
              setAssetTypeid={setAssetTypeid}
            />
          </Box>
        </DialogContent>
      </BootstrapDialog>
    </span>
  );
}
