import React, { useState, useEffect } from "react";
import DialogContent from "@mui/material/DialogContent";
import Typography from "@mui/material/Typography";
import DialogActions from "@mui/material/DialogActions";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { MenuItem, TextField } from "@mui/material";
import CreateCategory from "./CreateCategory";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import { Button } from "@mui/material";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import PropTypes from "prop-types";

const initial = () => ({
  assetType: "",
  price: "",
  purchaseon: null,
  model: "",
  attachment: "",
  description: "",
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

const PopupModel = ({
  open,
  error,
  handleClose,
  handelSubmit,
  Update,
  getAssetType,
  getAssetTypeApi,
}) => {
  // console.log("Update02222222",Update);
  const [assets, setAssets] = useState(initial());
  // console.log("------>assets",assets);

  useEffect(() => {
    if (Update && Object.keys(Update).length !== 0) {
      getValue(Update).then((attachment) => {
        setAssets((previous) => ({
          ...Update,
          attachment,
          previousAttach: attachment,
        }));
      });
    }

  }, [Update]); //eslint-disable-line

  // console.log("updateeee",Update.previousAttach, Update.attachment);

  const handlechangeAsset = (onChangeValue) => {
    const handleAsset = { ...assets };
    handleAsset.assetType = onChangeValue.target.value;
    setAssets(handleAsset);
  };

  const handlechangeModel = (onChangeValue) => {
    const handlemodel = { ...assets };
    handlemodel.model = onChangeValue.target.value;
    setAssets(handlemodel);
  };

  const handlechangePrice = (onChangeValue) => {
    const handleprice = { ...assets };
    handleprice.price = onChangeValue.target.value;
    setAssets(handleprice);
  };

  const handlechangeDes = (onChangeValue) => {
    const handleDes = { ...assets };
    handleDes.description = onChangeValue.target.value;
    setAssets(handleDes);
  };

  const handlechangePurc = (onChangeValue) => {
    const handlePurch = { ...assets };
    handlePurch.purchaseon = new Date(onChangeValue).toLocaleDateString();
    setAssets(handlePurch);
  };

  const handlechangeFile = (onChangeValue) => {
    const handleFile = { ...assets };
    handleFile.attachment = onChangeValue.target.files[0];
    setAssets(handleFile);
  };
  // -------------------------------------------------
  async function getFileFromUrl(url, name, defaultType = "image/jpeg") {
    const response = await fetch(url);
    const file = await new File([response], name, {
      type: defaultType,
    });
    return file;
  }

  // `await` can only be used in an async body, but showing it here for simplicity.
  const getValue = async (Update) => {
    if (Update.attachment && Update.attachment !== "") {
      // console.log("attachment", Update.attachment);
      if (Object.entries(Update).length) {
        const a = await getFileFromUrl(
          Update.attachment,
          Update.attachment.split("/").pop()
        );
        // console.log("linea", a);
        return a;
      }
      return assets.attachment;
    }
    return "";
  };
  // console.log(error, assets);
  // ----------------------------------------------------------------------------------
  return (
    <BootstrapDialog
      onClose={handleClose}
      open={open}
      
      PaperProps={{ sx: { minWidth: "300px" } }}
    >
      <BootstrapDialogTitle onClose={handleClose}>
        {Object.entries(Update).length ? "Update Asset" : "Creat Asset"}
      </BootstrapDialogTitle>
      <DialogContent dividers>
        <form onSubmit={(e) => handelSubmit(e, assets)}>
          <Typography gutterBottom>
            <div className="form-group form-group01">
              <TextField
                id="standard-basic"
                label="Asset Type"
                select
                // required
                sx={{ width: "90%" }}
                value={assets.assetType}
                onChange={(e) => handlechangeAsset(e)}
              >
                {getAssetType.map((d, i) => {
                  return (
                    <MenuItem value={d.assetstype} key={i}>
                      {d.assetstype}
                    </MenuItem>
                  );
                })}
              </TextField>

              <span>
                <CreateCategory
                  getAssetType={getAssetType}
                  getAssetTypeApi={getAssetTypeApi}
                />
              </span>

              <div className="error">
                {error && assets.assetType === ""
                  ? "Asset Type is required!"
                  : ""}
              </div>
            </div>

            <div className="row-f">
              <div className="col-f">
                <div className="form-group">
                  <TextField
                    id="standard-basic"
                    label="Price"
                    // required
                    type="number"
                    autoComplete="none"
                    sx={{ width: "100%" }}
                    value={assets.price}
                    onChange={(e) => handlechangePrice(e)}
                  />

                  <div className="error">
                    {error && assets.price === "" ? "Price is required!" : ""}
                  </div>
                </div>

                <div className="form-group">
                  <TextField
                    id="standard-basic"
                    label="Model"
                    // required
                    autoComplete="none"
                    sx={{ width: "100%" }}
                    value={assets.model}
                    onChange={(e) => handlechangeModel(e)}
                  />
                  <div className="error">
                    {error && assets.model === "" ? "Model is required!" : ""}
                  </div>
                  {/* <div className="error">err</div> */}
                </div>
              </div>
              <div className="col-f">
                <div className="form-group">
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      value={assets.purchaseon}
                      onChange={(e) => handlechangePurc(e)}
                      label="purchase on"
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          // required
                          sx={{ width: "100%" }}
                        />
                      )}
                    />
                  </LocalizationProvider>
                  <div className="error">
                    {error && assets.purchaseon === null
                      ? "Purchase On is required!"
                      : ""}
                  </div>
                  {/* <div className="error">err</div> */}
                </div>

                <div className="form-group attachment">
                  <label htmlFor={"icon-button-file"} className="input-file">
                    {assets.attachment ? "Update File" : "Choose File"}
                  </label>
                  <input
                    // accept={allowedFormats}
                    onChange={(e) => handlechangeFile(e)}
                    id={"icon-button-file"}
                    type="file"
                    style={{ display: "none" }}
                    multiple
                  />
                  <div className="file-update">
                    {assets.attachment ? <>{assets.attachment.name}</> : <></>}
                  </div>
                  <div className="error">
                    {error && assets.attachment === ""
                      ? "Attachment is required!"
                      : ""}
                  </div>
                </div>
              </div>
            </div>

            <div className="form-group">
              <TextField
                id="standard-basic"
                label="description"
                variant="outlined"
                sx={{ width: "100%" }}
                InputProps={{ sx: { height: 100 } }}
                // required
                value={assets.description}
                onChange={(e) => handlechangeDes(e)}
              />
              <div className="error">
                {error && assets.description === ""
                  ? "Description is required!"
                  : ""}
              </div>
              {/* <div className="error">err</div> */}
            </div>
          </Typography>

          <DialogActions>
            <Button type="submit">
              {Object.entries(Update).length ? "update" : "submit"}
              {/* submit */}
            </Button>
          </DialogActions>
        </form>
      </DialogContent>
    </BootstrapDialog>
  );
};

export default PopupModel;
