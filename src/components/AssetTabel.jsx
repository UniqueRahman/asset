import React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper,
} from "@mui/material";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import axios from "axios";
import PopupModel from "./PopupModel";
import Avatar from "@mui/material/Avatar";
import ViewImgPopup from "./ViewImgPopup";
// import VisibilityIcon from "@mui/icons-material/Visibility";

const AssetTabel = ({ item, getApi, getAssetType,getAssetTypeApi }) => {
  const [open, setOpen] = React.useState(-1);
  const [imgOpen, setImgOpen] = React.useState("");

  const handleEdit = (index) => {
    setOpen(index);
  };

  const handleClose = () => {
    setOpen(-1);
  };
  const handleViewOpen = (attachment) => {
    setImgOpen(attachment);
  };
  const handleViewClose = () => {
    setImgOpen(false);
  };
  //-----------------Delete----------------------------------->
  const handelDelete = (assetId) => {
    axios
      .delete(`http://localhost:4000/asset/deleteasset?assetId=${assetId}`)
      .then((res) => {
        getApi();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  //-----------------update------------------------------------->
  const handelSubmit = (e, assets) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("assetType", assets.assetType);
    formData.append("price", assets.price);
    formData.append("purchaseon", assets.purchaseon);
    formData.append("model", assets.model);
    formData.append("description", assets.description);
    formData.append("assetId", assets._id);
    if (assets.attachment !== assets.previousAttach) {
      formData.append("attachment", assets.attachment);
    }

    axios
      .put("http://localhost:4000/asset/updateasset", formData)
      .then((res) => {
        handleClose();
        getApi();
      })
      .catch((err) => console.log(err));
  };

  return (
    <div>
      <TableContainer
        component={Paper}
        sx={{ marginTop: "40px", maxHeight: "500px" }}
      >
        <Table aria-label="simple table" stickyHeader sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Asset Type</TableCell>
              <TableCell>Purchase on</TableCell>
              <TableCell>price</TableCell>
              <TableCell>Model</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Attachment</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {item.map((Assetdata, index) => {
              return (
                <TableRow key={index}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{Assetdata.assetType}</TableCell>
                  <TableCell>{Assetdata.purchaseon}</TableCell>
                  <TableCell>{Assetdata.price}</TableCell>
                  <TableCell>{Assetdata.model}</TableCell>
                  <TableCell>{Assetdata.description}</TableCell>
                  <TableCell>
                    {/* {console.log("Assetdata.attachment", Assetdata.attachment)} */}
                    <Tooltip title="view Image" arrow placement="top">
                      <IconButton>
                        <Avatar
                          alt="Remy Sharp"
                          src={Assetdata.attachment}
                          sx={{ cursor: "pointer" }}
                          onClick={() => handleViewOpen(Assetdata.attachment)}
                        />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                  <TableCell>
                    <Tooltip title="Edit" arrow placement="top">
                      <IconButton
                        onClick={() => handleEdit(index)}
                        sx={{ marginRight: "20px" }}
                      >
                        <BorderColorIcon color="success" />
                      </IconButton>
                    </Tooltip>

                    <Tooltip title="Delete" arrow placement="top">
                      <IconButton onClick={() => handelDelete(Assetdata._id)}>
                        <DeleteForeverIcon color="error" />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

      {open !== -1 && (
        <PopupModel
          open={open !== -1}
          handleClose={handleClose}
          getApi={getApi}
          Update={item[open]}
          getAssetType={getAssetType}
          handelSubmit={handelSubmit}
          getAssetTypeApi={getAssetTypeApi}
        />
      )}

      {imgOpen && (
        <ViewImgPopup handleViewClose={handleViewClose} imgOpen={imgOpen} />
      )}
    </div>
  );
};

export default AssetTabel;
