import React from "react";
import Avatar from "@mui/material/Avatar";
import Typography  from "@mui/material/Typography";

const Header = () => {
  return (
    <div>
      <Typography 
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          height: 70,
        }}
      >
        {/* <h3>Digival</h3> */}
        <img
          src="https://www.digi-val.com/assets/img/logo.svg"
          alt="Digival IT Solutions Logo"
          class="img-fluid"
          className="logoimg"
        />
        <Avatar alt="Remy Sharp" />
      </Typography >
    </div>
  );
};

export default Header;
