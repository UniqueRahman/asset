import React, { useEffect, useState } from "react";
import CreateAssets from "./CreateAssets";
import axios from "axios";
import AssetTabel from "./AssetTabel";

const Assets = () => {
  const [items, setItems] = useState([]);
  
  const [getAssetType, setGetAssetType] = React.useState([]);
  // console.log("item",items)
  
  const getApi = () => {
    axios
      .get("http://localhost:4000/asset/retriveasset")
      .then((res) => {
        setItems(res.data.message);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  
  const getAssetTypeApi = () => {
    axios
      .get("http://localhost:4000/asset/categoriesdata")
      .then((res) => {
        setGetAssetType(res.data.message);
      })
      .catch((err) => {
      });
  };

  useEffect(() => {
    getApi();
    getAssetTypeApi();
  }, []);

  return (
    <div>
      <div className="Assets-header">
        <CreateAssets getApi={getApi} getAssetType={getAssetType} getAssetTypeApi={getAssetTypeApi} setGetAssetType={setGetAssetType}/>
      </div>
      <div className="Assets-main">
        <AssetTabel item={items} getApi={getApi} getAssetType={getAssetType} getAssetTypeApi={getAssetTypeApi}/>
      </div>
    </div>
  );
};

export default Assets;
