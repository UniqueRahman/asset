import React from "react";
import Header01 from "../Header01";
import SideNavigation02 from "../SideNavigation02";
import Assets03 from "../Assets03";
import "../AssetManagement/AssetManagement.css";
import {Routes, Route } from "react-router-dom";

const AssetManagement = () => {
  return (
    <div className="body">
      <header>
        <Header01 />
      </header>
      <aside>
        <SideNavigation02 />
      </aside>
      <main>
        <Routes>
          <Route path="/asset" element={<Assets03/>}></Route>
        </Routes>
      </main>
    </div>
  );
};

export default AssetManagement;
