import React from "react";
import AssetManagement from "./components/AssetManagement/AssetManagement";
const App = () => {
  return (
    <div>
      <AssetManagement />
    </div>
  );
};

export default App;
