async function getFileFromUrl(url, name, defaultType = 'image/jpeg'){
    const response = await fetch(url);
    const data = await response.blob();
    return new File([data], name, {
      type: data.type || defaultType,
    });
  }
  
  // `await` can only be used in an async body, but showing it here for simplicity.
  const file =  getFileFromUrl('https://example.com/image.jpg', 'example.jpg')
  // console.log(file);

// function previewFile(file) {

//     var reader  = new FileReader();
  
//     reader.onloadend = function () {
//       console.log(reader.result); //this is an ArrayBuffer
//     }
//     reader.readAsArrayBuffer(file);
//   }
//   previewFile(file)

// async function createFile(){
//     let response = await fetch('http://127.0.0.1:8080/test.jpg');
//     let data = await response.blob();
//     let metadata = {
//       type: 'image/jpeg'
//     };
//     let file = new File([data], "test.jpg", metadata);
//     console.log(file);
//     // ... do something with the file or return it
//   }
// createFile();
